require 'test_helper'

class WorkingOnsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @working_on = working_ons(:one)
  end

  test "should get index" do
    get working_ons_url
    assert_response :success
  end

  test "should get new" do
    get new_working_on_url
    assert_response :success
  end

  test "should create working_on" do
    assert_difference('WorkingOn.count') do
      post working_ons_url, params: { working_on: { issue_id: @working_on.issue_id, user_id: @working_on.user_id } }
    end

    assert_redirected_to working_on_url(WorkingOn.last)
  end

  test "should show working_on" do
    get working_on_url(@working_on)
    assert_response :success
  end

  test "should get edit" do
    get edit_working_on_url(@working_on)
    assert_response :success
  end

  test "should update working_on" do
    patch working_on_url(@working_on), params: { working_on: { issue_id: @working_on.issue_id, user_id: @working_on.user_id } }
    assert_redirected_to working_on_url(@working_on)
  end

  test "should destroy working_on" do
    assert_difference('WorkingOn.count', -1) do
      delete working_on_url(@working_on)
    end

    assert_redirected_to working_ons_url
  end
end
