class Status < ApplicationRecord
  has_many :substatuses, class_name: "Status", foreign_key: "parent_id"

  belongs_to :parent, class_name: "Status", optional: true
end
