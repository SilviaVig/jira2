class Issue < ApplicationRecord
  default_scope { order(priority: :desc) }
  belongs_to :project
  belongs_to :status
  has_many :comments, :dependent => :destroy
  has_one :working_on, :dependent => :destroy
  has_one :user, through: :working_on
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true
  validates :priority, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 5}
  validates :status, presence: true
  validates :project, presence: true
end