class User < ApplicationRecord
  belongs_to :group, optional:true
  has_many :working_ons, :dependent => :destroy
  has_many :issues, through: :working_ons
  has_secure_password
  validates :username, presence: true, uniqueness: true, length: { maximum: 50 }
end
