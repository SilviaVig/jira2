class WorkingOnsController < ApplicationController
  before_action :set_working_on, only: [:show, :edit, :update, :destroy]

  # GET /working_ons
  # GET /working_ons.json
  def index
    @working_ons = WorkingOn.all
  end

  # GET /working_ons/1
  # GET /working_ons/1.json
  def show
  end

  # GET /working_ons/new
  def new
    @working_on = WorkingOn.new
  end

  # GET /working_ons/1/edit
  def edit
  end

  # POST /working_ons
  # POST /working_ons.json
  def create
    authorize! :create, @working_on
    @working_on = WorkingOn.new(working_on_params)

    respond_to do |format|
      if @working_on.save
        format.html { redirect_to @working_on, notice: 'Working on was successfully created.' }
        format.json { render :show, status: :created, location: @working_on }
      else
        format.html { render :new }
        format.json { render json: @working_on.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /working_ons/1
  # PATCH/PUT /working_ons/1.json
  def update
    authorize! :update, @working_on
    respond_to do |format|
      if @working_on.update(working_on_params)
        format.html { redirect_to @working_on, notice: 'Working on was successfully updated.' }
        format.json { render :show, status: :ok, location: @working_on }
      else
        format.html { render :edit }
        format.json { render json: @working_on.errors, status: :unprocessable_entity }
      end
    end
  end

  def change_assignee
    params.permit(:issue_id, :user_id)
    issue_id = params[:issue_id]
    wanted_user_id = params[:user_id]
    @working_on = WorkingOn.where(issue_id: issue_id).first_or_initialize
    respond_to do |format|
      if @working_on.update_attribute(:user_id, wanted_user_id)
        format.html { redirect_to issue_path(@working_on.issue), notice: 'Working on was successfully updated.' }
        format.json { render :show, status: :ok, location: @working_on }
      else
        format.html { render :edit }
        format.json { render json: @working_on.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /working_ons/1
  # DELETE /working_ons/1.json
  def destroy
    authorize! :destroy, @working_on
    @working_on.destroy
    respond_to do |format|
      format.html { redirect_to working_ons_url, notice: 'Working on was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_working_on
    @working_on = WorkingOn.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def working_on_params
    params.require(:working_on).permit(:user_id, :issue_id)
  end
end
