class CommentsController < ApplicationController
  load_and_authorize_resource
  before_action :find_issue
  before_filter :authorize
  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.all
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = @issue.comments.create(params[:comment].permit(:content))
    @comment.user_id = current_user.id
    if @comment.save
        redirect_to issue_path(@issue)
    else
      render 'new'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:content, :user_id, :issue_id)
    end

  def find_issue
    @issue = Issue.find(params[:issue_id])
  end
end
