class IssuesController < ApplicationController
  load_and_authorize_resource
  before_action :set_issue, only: [:show, :edit, :update, :destroy, :edit_status, :change_status]
  before_filter :authorize
  # GET /issues
  # GET /issues.json
  def index
    @issues = Issue.all
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
    @substatuses = Array.new
    @substatuses = @substatuses.append(@issue.status)
    @substatuses = @substatuses|@issue.status.substatuses
    @issue_user_id = nil
    if (!@issue.user.nil?)
      @issue_user_id = @issue.user.id
    end
    @comments = Comment.where(issue_id: @issue.id).order('created_at DESC')
  end

  # GET /issues/new
  def new
    @issue = Issue.new
  end

  # GET /issues/1/edit
  def edit
  end

  # POST /issues
  # POST /issues.json
  def create
    @issue = Issue.new(issue_params)

    respond_to do |format|
      if @issue.save
        format.html { redirect_to @issue, notice: 'Issue was successfully created.' }
        format.json { render :show, status: :created, location: @issue }
      else
        format.html { render :new }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    respond_to do |format|
      if @issue.update(issue_params)
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    @issue.destroy
    respond_to do |format|
      format.html { redirect_to issues_url, notice: 'Issue was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def edit_status
  end

  def change_status
    substatuses = @issue.status.substatuses
    if !substatuses.nil?
      allowed_statuses = substatuses.map { |status| status.id }
      params.permit[:status_id]
      wanted_status_id = params[:status_id].to_i
      if allowed_statuses.include? wanted_status_id
        respond_to do |format|
          if @issue.update_attribute(:status_id, wanted_status_id)
            format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
            format.json { render :show, status: :ok, location: @issue }
          else
            format.html { render :edit }
            format.json { render json: @issue.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_issue
    @issue = Issue.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def issue_params
    params.require(:issue).permit(:name, :description, :priority, :project_id, :status_id)
  end
end
