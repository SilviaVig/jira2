json.extract! issue, :id, :name, :description, :priority, :project_id, :status_id, :created_at, :updated_at
json.url issue_url(issue, format: :json)