class CreateWorkingOns < ActiveRecord::Migration[5.0]
  def change
    create_table :working_ons do |t|
      t.references :user, foreign_key: true
      t.references :issue, foreign_key: true

      t.timestamps
    end
  end
end
