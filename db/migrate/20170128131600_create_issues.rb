class CreateIssues < ActiveRecord::Migration[5.0]
  def change
    create_table :issues do |t|
      t.string :name
      t.text :description
      t.integer :priority, default: 0
      t.references :project, foreign_key: true
      t.references :status, foreign_key: true

      t.timestamps
    end
  end
end
