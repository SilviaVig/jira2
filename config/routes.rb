Rails.application.routes.draw do
  root :to => 'users#show_issues'

  resources :issues do
    resources :comments
  end
  resources :statuses
  resources :users do
    resources :issues
  end
  resources :projects
  resources :groups do
    resources :users
  end

  get '/issues/:id/edit_status' => 'issues#edit_status', as: 'edit_issue_status'
  put '/issues/:id/change_status' => 'issues#change_status', as: 'change_issue_status'
  put '/working_ons/:id/change_assignee' => 'working_ons#change_assignee', as: 'change_assignee'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy', as: 'logout'

  get '/signup' => 'users#new'
  post '/users' => 'users#create'

  get 'users/:id/show_issues' => 'users#show_issues', as: 'show_issues'
end
